/******************************************************************************
Utility to strip the Prism II headers from a tracefile

Copyright (C) 2013 James Swaro <james.swaro@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.

Author: James Swaro <james.swaro@gmail.com>
Date: 06/2013
******************************************************************************/
#include "prismstrip.h"

static char errstr[1024];
static u_char usrstr[1024];
static pcap_t* in;			/*libpcap input file discriptor */
static pcap_t* tmp;			/*libpcap fake file discriptor */
static pcap_dumper_t* out;	/*libpcap output file discriptor*/
static u_int32_t pkt_count; 

void version();
void usage();
void strip_prism_headers(u_char *user, const struct pcap_pkthdr *h,
							const u_char *bytes); 
                                  
void version()
{
	fprintf(stdout, "prismstrip version %.1f\n",PRISMSTRIP_VERSION);
	fprintf(stdout, "Copyright (C) %i James Swaro <james.swaro@gmail.com>\n",COPYRIGHT_YEAR);
	fprintf(stdout, "This program comes with ABSOLUTELY NO WARRANTY.\n");
	fprintf(stdout, "This is free software, and you are welcome to\n");
	fprintf(stdout, "redistribute it under certain conditions.\n");
}

void usage() 
{
	version();
	
	printf("Usage: prismstrip <tracefile>\n");
}



int main(int argc, char **argv) 
{
	int ret = 0;
	
	if (argc != 2) 
	{
		usage();	
		exit(EXIT_FAILURE);
	}

	// Assume the only argument is a file

	char infile[1024];
	char outfile[1024];
	strcpy((char *) &infile, argv[1]);
	strcpy((char *) &outfile, argv[1]);
	strcat(outfile, (const char*) ".strip");
	
	FILE *ofp = fopen((char *) outfile, "w");

	in = pcap_open_offline(infile, errstr);
	
	if (!in) 
	{
		fprintf(stderr, "failed to open tracefile: %s\n", infile);
		exit(EXIT_FAILURE);
	}
	
	/*attempt to open output file*/
	tmp = pcap_open_dead(DLT_RAW,65535);
	out = pcap_dump_fopen(tmp,ofp);
	
	if (!tmp) 
	{
		fprintf(stderr, "failed to open tmp descriptor\n");
		exit(EXIT_FAILURE);
	}
	
	if (!out)
	{
		fprintf(stderr, "failed to open stripfile: %s\n", outfile);
		exit(EXIT_FAILURE);
	}
	
	/*process packets*/
	u_char *user = (u_char*) out;
	if ((ret = pcap_loop(in, -1, 
				(pcap_handler) strip_prism_headers, user)) == PCAP_ERROR) 
	{
		fprintf(stderr, "main: %s:%s", errstr, usrstr);
	} 
	else if (ret == PCAP_ERROR_BREAK) 
	{
		fprintf(stderr, "main: %s:%s", errstr, usrstr);
	} 
	else 
	{
		fprintf(stdout, "%i packets stripped\n", pkt_count);
		ret = 0;
	}
	
	/*close files*/
	pcap_close(in);
	pcap_dump_close(out);
	pcap_close(tmp);
	
	return ret;
}

void strip_prism_headers(u_char *user, const struct pcap_pkthdr *h,
                                   const u_char *bytes)
{
	int  hdrlen = (PRISM_HDRLEN + WLAN_HDRLEN + LLC_SNAPLEN);
	
	if (pcap_datalink(in) != DLT_PRISM_HEADER) 
		return;
		
	if (h->caplen < hdrlen)
		return;
	
	wlan_header_t *wireless_hdr = (wlan_header_t*) &bytes[PRISM_HDRLEN];
	llc_snap_t  *llc_snap_hdr = (llc_snap_t*) &bytes[PRISM_HDRLEN + WLAN_HDRLEN];
	
	if (wireless_hdr->data[0] != 0x08) // not a 802.11 data frame
		return;
		
	if (llc_snap_hdr->dsap == 0xaa && llc_snap_hdr->ssap == 0xaa &&
		llc_snap_hdr->control == 0x03) {
		struct pcap_pkthdr new;
		
		new.ts = h->ts;
		new.caplen = h->caplen - hdrlen;
		new.len = h->len - hdrlen;
		bytes +=  hdrlen;			

		pcap_dump(user, &new, bytes);
		
		pkt_count++;
	}
	return;
}

