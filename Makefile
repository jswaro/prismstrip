###############################################################################
# Author: James Swaro
#
# Date: 6/2013
#
# Makefile for program prismstrip
###############################################################################

CFLAGS= -O2 -Wall -Werror -g

LDLIBS= -lpcap

BINDIR= /usr/local/bin
MANDIR= /usr/local/man


all: prismstrip prismstrip.1

prismstrip: prismstrip.c prismstrip.h
	gcc ${CFLAGS} --std=gnu99 prismstrip.c -o prismstrip ${LDLIBS}

prismstrip.1: prismstrip.pod
	pod2man -s 1 -c "prismstrip" prismstrip.pod > prismstrip.1

install: prismstrip
	install -m 755 -o bin -g bin prismstrip ${BINDIR}/prismstrip
	install -m 444 -o bin -g bin prismstrip.1 ${MANDIR}/man1/prismstrip.1

uninstall:
	rm -f ${BINDIR}/prismstrip
	rm -f ${MANDIR}/man1/prismstrip.1

clean:
	rm -f *~ prismstrip core *.o prismstrip.1

