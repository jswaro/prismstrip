#ifndef PRISMSTRIP_H
#define PRISMSTRIP_H

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <pcap/pcap.h>
#include "prismstrip.h"
#include <string.h>

#define PRISMSTRIP_VERSION 0.1
#define COPYRIGHT_YEAR 2013
#define PRISM_HDRLEN 144
#define WLAN_HDRLEN 24
#define LLC_SNAPLEN 8

typedef struct prism_header {
	u_char data[144];
} prism_header_t;

typedef struct wlan_header {
	u_char data[24];
} wlan_header_t;

typedef struct llc_snap {
	u_char dsap;
	u_char ssap;
	u_char control;
	u_char uid[3];
	u_char protocol[2];
} llc_snap_t;
#endif

